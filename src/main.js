import Vue from "vue";
import router from "./router";
import vuesax from "vuesax";
import "vuesax/dist/vuesax.css";
import "boxicons/css/boxicons.min.css";
import App from "./App.vue";
import "./registerServiceWorker";

Vue.config.productionTip = false;
Vue.use(vuesax);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
